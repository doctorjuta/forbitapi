"""Scrapping library for coingecko API service - Cryptocurrencies.

Path to request URL: https://api.coingecko.com/api/v3/coins
Documentation: https://www.coingecko.com/api/docs/v3
Date of creating: 30/10/2018
"""
from scrapers.scraper_obj import ForbitScrapObj
import time
from db import ForbitDB
from multiprocessing.dummy import Pool as ThreadPool


class Scraper(ForbitScrapObj):
    """Main scrapping class for coingecko API."""

    service_name = "Coingecko Cryptocurrencies"
    api_path = "https://api.coingecko.com/api/v3/coins"

    def __init__(self):
        """Init parent class."""
        ForbitScrapObj.__init__(self)
        self.db = ForbitDB()

    def format_float_number(self, number):
        """Format float number to correct DB form."""
        if number is None or number == "None":
            number = ""
        else:
            number = str(number)
        if "e-" in number:
            tmp_number = number.split("e-")
            number = tmp_number[0]
        if "." in number:
            tmp_number = number.split(".")
            number = "{}.{}".format(tmp_number[0], tmp_number[1][0:6])
        return number

    def format_string(self, str):
        """Format string to correct DB form."""
        new_str = str.lower()
        return new_str.replace("'", "")

    def get_raw_data_singe(self, page):
        """Single script iteration in multithread stream."""
        next_page = "{}?per_page=250&page={}".format(
            self.api_path,
            page
        )
        tmp_raw_data = self.make_request(next_page)
        if not tmp_raw_data:
            return
        try:
            tmp_raw_data = tmp_raw_data.json()
        except ValueError as error:
            tmp_raw_data = False
            mess = "Error on parsing JSON for service {}: {}.".format(
                self.service_name,
                error
            )
            self.logger.error(mess)
        time.sleep(.1)
        # print("Coins parsed on page: {}".format(page))
        if tmp_raw_data and isinstance(tmp_raw_data, list):
            if not self.raw_data:
                self.raw_data = []
            self.raw_data += tmp_raw_data
        else:
            return

    def get_raw_data(self):
        """Get raw data from api_path."""
        pages = [i for i in range(1, 15)]
        pool = ThreadPool(5)
        pool.map(
            self.get_raw_data_singe,
            pages
        )
        pool.close()
        pool.join()

    def get_format_data(self):
        """Prepare raw data for storing in DB."""
        format_data = []
        coins_with_api = self.db.get_cryptocurrencies()
        if not self.raw_data:
            return
        if "error" in self.raw_data:
            mess = "Error on getting data for serivce {}: {}.".format(
                self.service_name,
                self.raw_data["error"]
            )
            self.logger.error(mess)
            return format_data
        for el in self.raw_data:
            coin_slug = self.format_string(el["symbol"])
            if coin_slug in coins_with_api:
                cd = {
                    "id": self.format_string(el["id"]),
                    "symbol": self.format_string(el["symbol"]),
                    "name": self.format_string(el["name"]),
                    "price_usd": 0,
                    "price_rub": 0,
                    "price_btc": 0,
                    "cap_usd": 0,
                    "cap_usd_change_24": 0,
                    "change_24h_prc": 0,
                    "change_24h_usd": 0,
                    "change_7d_prc": 0,
                    "circulating_supply": 0,
                    "total_supply": 0
                }
                if "market_data" in el:
                    m = el["market_data"]
                    if "current_price" in m:
                        c = m["current_price"]
                        if "usd" in c:
                            cd["price_usd"] = self.format_float_number(
                                c["usd"]
                            )
                        if "rub" in c:
                            cd["price_rub"] = self.format_float_number(
                                c["rub"]
                            )
                        if "btc" in c:
                            cd["price_btc"] = self.format_float_number(
                                c["btc"]
                            )
                    if "market_cap" in m:
                        if "usd" in m["market_cap"]:
                            cd["cap_usd"] = self.format_float_number(
                                m["market_cap"]["usd"]
                            )
                    if "market_cap_change_24h_in_currency" in m:
                        if "usd" in m["market_cap_change_24h_in_currency"]:
                            cd["cap_usd_change_24"] = self.format_float_number(
                                m["market_cap_change_24h_in_currency"]["usd"]
                            )
                    if "price_change_percentage_24h" in m:
                        cd["change_24h_prc"] = self.format_float_number(
                            m["price_change_percentage_24h"]
                        )
                    if "price_change_24h" in m:
                        cd["change_24h_usd"] = self.format_float_number(
                            m["price_change_24h"]
                        )
                    if "price_change_percentage_7d" in m:
                        cd["change_7d_prc"] = self.format_float_number(
                            m["price_change_percentage_7d"]
                        )
                    if "circulating_supply" in m and m["circulating_supply"]:
                        cd["circulating_supply"] = m["circulating_supply"]
                    if "total_supply" in m and m["total_supply"]:
                        cd["total_supply"] = m["total_supply"]
                format_data.append(cd)
        self.format_data = format_data
