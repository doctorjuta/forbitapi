"""Scrapping library for coingecko API service - Exchanges.

Path to request URL:
    list of exchanges - https://api.coingecko.com/api/v3/exchanges/
    individual exchange - https://api.coingecko.com/api/v3/exchanges/bitforex
Documentation: https://www.coingecko.com/api/docs/v3
Date of creating: 01/11/2018
"""
from scrapers.scraper_obj import ForbitScrapObj
import time
from db import ForbitDB
from multiprocessing.dummy import Pool as ThreadPool


class Scraper(ForbitScrapObj):
    """Main scrapping class for coingecko API."""

    service_name = "Coingecko Exchanges"
    api_path = "https://api.coingecko.com/api/v3/exchanges"
    data_type = "exchanges"
    possible_exchanges = False

    def __init__(self):
        """Init parent class."""
        ForbitScrapObj.__init__(self)
        self.db = ForbitDB()

    def prepare_requests(self):
        """Check which exchanges we can parse with current service."""
        raw_data = False
        if self.api_path:
            raw_data = self.make_request(self.api_path)
        if raw_data:
            raw_data = raw_data.json()
        if raw_data and isinstance(raw_data, list) and len(raw_data) > 0:
            self.possible_exchanges = set()
            for exchange in raw_data:
                self.possible_exchanges.add(exchange["id"])

    def get_possible_exchanges(self, db_exchanges):
        """Return list of exchanges id which will be parsed."""
        if len(db_exchanges) < 1:
            db_exchanges = set(self.db.get_exchanges())
        exchanges_to_parse = db_exchanges
        if self.possible_exchanges and len(self.possible_exchanges) > 0:
            exchanges_to_parse = db_exchanges.intersection(
                self.possible_exchanges
            )
        return exchanges_to_parse

    def format_string(self, str):
        """Format string to correct DB form."""
        new_str = str.lower()
        return new_str.replace("'", "")

    def calculate_each_par_vol_perc(self, data):
        """Calculate volume percent for each pair of exchanges."""
        if len(data["pairs"]) > 0 and data["volume"] > 0:
            for row in data["pairs"]:
                row["volume_percent"] = self.calculate_par_vol_perc(
                    data["volume"],
                    row
                )

    def calculate_par_vol_perc(self, volume, data):
        """Calculate volume percent for individual pair."""
        if data["volume"] > 0:
            return round((100*data["volume"])/volume, 6)
        else:
            return 0

    def get_raw_data(self, exchange_id):
        """Get raw data from api_path."""
        raw_data = False
        if self.api_path:
            raw_data = self.get_raw_data_pairs(exchange_id)
        return raw_data

    def get_raw_data_pairs(self, exchange_id):
        """Get pairs raw data from api_path by provided exchange id."""
        page = 1
        raw_pairs = []
        while True:
            next_page = "{}/{}/tickers?page={}".format(
                self.api_path,
                exchange_id,
                page
            )
            pairs_raw_data = self.make_request(next_page)
            if not pairs_raw_data:
                break
            try:
                pairs_raw_data = pairs_raw_data.json()
            except ValueError as error:
                pairs_raw_data = []
                mess = (
                    "Error on parsing pairs JSON for exchange {} "
                    "(service {}): {}."
                ).format(
                    exchange_id,
                    self.service_name,
                    error
                )
                self.logger.error(mess)
            time.sleep(.1)
            # print("Coingecko: parsed new pairs for {}, page {}".format(exchange_id, page))
            if (
                pairs_raw_data
                and "tickers" in pairs_raw_data
                and len(pairs_raw_data["tickers"]) > 0
            ):
                raw_pairs += pairs_raw_data["tickers"]
            else:
                break
            page += 1
        return raw_pairs

    def get_format_data(self, exchange_id, raw_data):
        """Prepare raw data for storing in DB."""
        format_data = False
        if raw_data and len(raw_data) > 0:
            format_data = {
                "exchange": self.format_string(exchange_id),
                "pairs": [],
                "volume": 0
            }
            volume = 0
            for el in raw_data:
                currency_name = self.format_string(el["base"])
                new_par = {
                    "base": currency_name,
                    "target": self.format_string(el["target"]),
                    "name": currency_name,
                    "price_usd": 0,
                    "volume": 0,
                    "volume_percent": 0
                }
                if "last" in el:
                    price_usd = el["last"]
                    if price_usd:
                        price_usd = round(
                            float(price_usd),
                            6
                        )
                    else:
                        price_usd = 0
                    new_par["price_usd"] = price_usd
                if (
                    "converted_volume" in el
                    and "usd" in el["converted_volume"]
                ):
                    volume_usd = el["converted_volume"]["usd"]
                    if volume_usd:
                        volume_usd = round(float(volume_usd), 2)
                    else:
                        volume_usd = 0
                    new_par["volume"] = volume_usd
                    volume += volume_usd
                format_data["pairs"].append(new_par)
            format_data["volume"] = volume
            self.calculate_each_par_vol_perc(format_data)
        return format_data

    def get_data_single(self, exchange_id):
        """Single script iteration in multithread stream."""
        raw_data = self.get_raw_data(exchange_id)
        if raw_data:
            self.format_data[exchange_id] = self.get_format_data(
                exchange_id,
                raw_data
            )
            if self.format_data[exchange_id]:
                self.formated_exchanges_id.add(exchange_id)

    def get_data(self, db_exchanges=set()):
        """Start main script."""
        exchanges_to_parse = self.get_possible_exchanges(db_exchanges)
        self.format_data = {}
        self.formated_exchanges_id = set()
        pool = ThreadPool(5)
        pool.map(
            self.get_data_single,
            exchanges_to_parse
        )
        pool.close()
        pool.join()
        return {
            "data_type": self.data_type,
            "target": self.service_name,
            "data": self.format_data,
            "timestamp": int(time.time()),
            "formated_exchanges_id": self.formated_exchanges_id
        }
