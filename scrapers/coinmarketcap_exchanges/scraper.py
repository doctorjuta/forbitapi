"""Scrapping library for coinmarketcap service (html) - Exchanges.

Path to request URL:
    list of exchanges - https://coinmarketcap.com/exchanges/
    individual exchange - https://coinmarketcap.com/exchanges/okex/
Date of creating: 05/11/2018
"""
from scrapers.scraper_obj import ForbitScrapObj
import time
import urllib.parse
import urllib.request
import urllib.error
from bs4 import BeautifulSoup


class Scraper(ForbitScrapObj):
    """Main scrapping class for coinmarketcap html service."""

    service_name = "Coinmarketcap Exchanges"
    api_path = "https://coinmarketcap.com/exchanges"
    data_type = "exchanges"
    HEADERS = {
        "User-Agent": (
            "Mozilla/5.0 (X11; Ubuntu; Linux "
            "x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"
        )
    }

    def __init__(self):
        """Init parent class."""
        ForbitScrapObj.__init__(self)

    def get_raw_data(self, exchange_id):
        """Get raw data from api_path."""
        source = False
        target_url = '{}/{}'.format(self.api_path, exchange_id)
        try:
            request = urllib.request.Request(target_url, headers=self.HEADERS)
            source = urllib.request.urlopen(request).read()
        except urllib.error.URLError as e:
            mess = (
                "Can not connect to API site: {}. "
                "Problem with network connection: {}"
            ).format(
                target_url,
                e.reason
            )
            self.logger.error(mess)
        except urllib.error.HTTPError as e:
            mess = (
                "Can not connect to API site: {}. Server "
                "return {} error code. Reason: {}"
            ).format(
                target_url,
                e.code,
                e.reason
            )
            self.logger.error(mess)
        time.sleep(5)
        # print("Coinmarketcap: parsed new pairs for {}".format(exchange_id))
        return source

    def get_format_data(self, exchange_id, raw_data):
        """Prepare raw data for storing in DB."""
        format_data = {
            "exchange": exchange_id,
            "pairs": [],
            "volume": 0
        }
        pairs = []
        soup = BeautifulSoup(raw_data, "html.parser")
        table = soup.find(id="exchange-markets")
        table_body = False
        if table:
            table_body = table.find("tbody")
        if table_body:
            trs = table_body.find_all("tr")
            for tr in trs:
                tds = tr.find_all("td")
                currency_name = tds[1]["data-sort"].replace(
                    "'", ""
                ).replace(
                    '"', ''
                )
                pair = tds[2]["data-sort"].split("/")
                pairs.append(
                    {
                        "base": pair[0].lower(),
                        "target": pair[1].lower(),
                        "name": currency_name,
                        "price_usd": tds[4]["data-sort"],
                        "volume": tds[3]["data-sort"],
                        "volume_percent": tds[5]["data-sort"]
                    }
                )
        if len(pairs) > 0:
            format_data["pairs"] = pairs
        full_volume = soup.select(".text-left span.h2")
        if not full_volume:
            full_volume = 0
        else:
            full_volume = full_volume[0].get_text().replace(
                "$", ""
            ).replace(
                ",", ""
            )
        format_data["volume"] = full_volume
        return format_data

    def get_data(self, db_exchanges=set()):
        """Start main script."""
        format_data = {}
        formated_exchanges_id = set()
        for exchange_id in db_exchanges:
            raw_data = self.get_raw_data(exchange_id)
            if raw_data:
                format_data[exchange_id] = self.get_format_data(
                    exchange_id,
                    raw_data
                )
                if format_data[exchange_id]:
                    formated_exchanges_id.add(exchange_id)
        self.format_data = format_data
        return {
            "data_type": self.data_type,
            "target": self.service_name,
            "data": self.format_data,
            "timestamp": int(time.time()),
            "formated_exchanges_id": formated_exchanges_id
        }
