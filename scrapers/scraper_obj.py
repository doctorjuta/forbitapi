"""Scrapping library main object.

Current library provide main class for all scrapers.
"""
from logger import ForbitLogs
import requests
from requests.exceptions import ConnectionError, HTTPError
import time


class ForbitScrapObj:
    """Simple scraper class for all services."""

    api_path = False
    service_name = "NonInit"
    raw_data = False
    format_data = False
    data_type = "coins"

    def __init__(self):
        """Init main object."""
        self.logger = ForbitLogs(__name__)

    def make_request(self, path):
        """Get data from provided path."""
        data = False
        try:
            data = requests.get(path)
        except ConnectionError as e:
            mess = (
                "A Connection error occurred: {}."
            ).format(
                e
            )
            self.logger.error(mess)
        except HTTPError as e:
            mess = (
                "An HTTP error occurred: {}."
            ).format(
                e
            )
            self.logger.error(mess)
        if data and data.status_code == 429:
            mess = "Too Many Requests error on service {}.".format(
                self.service_name
            )
            self.logger.error(mess)
        return data

    def get_raw_data(self):
        """Get raw data from api_path."""
        if self.api_path:
            self.raw_data = self.make_request(self.api_path)

    def get_format_data(self):
        """Prepare raw data for storing in DB."""
        if self.raw_data:
            self.format_data = self.raw_data

    def get_data(self, db_exchanges=set()):
        """Start main script."""
        self.get_raw_data()
        self.get_format_data()
        return {
            "data_type": self.data_type,
            "target": self.service_name,
            "data": self.format_data,
            "timestamp": int(time.time())
        }
