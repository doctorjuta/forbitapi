"""Scrapping library.

Current library provide easy way to getting data from different sources
(API, html pages, etc) and return them to main script in correct format.
"""
import importlib
from db import ForbitDB


class ForbitScrap:
    """Main scrapping class."""

    services_queue = [
        "coingecko_coins",
        "coingecko_exchanges",
        "coinmarketcap_exchanges"
    ]
    services = {
        "coingecko_coins": False,
        "coingecko_exchanges": False,
        "coinmarketcap_exchanges": False
    }

    """db_exchanges - contain all exchanges from database. When scraper get
    formated data for individual exchange - we remove this exchange from
    db_exchanges list. As result - at finished we always know which
    exchanges from DB did not get new data."""
    db_exchanges = []

    def __init__(self):
        """Init main class."""
        self.format_data = {}
        self.db = ForbitDB()
        self.db_exchanges = set(self.db.get_exchanges())

    def import_scrapers(self):
        """Import all scrappers libs and create target object."""
        for service in self.services_queue:
            lib = "scrapers.{}.scraper".format(service)
            module = importlib.import_module(lib)
            self.services[service] = module.Scraper()

    def prepare_requests_data(self):
        """Prepare request for each service.

        This method is helpfull if you want to use threading and you need
        service get data only for specific exchanges or coins. You can
        setup this lists of exchanges/coins in this method.
        """
        for service in self.services_queue:
            scraper = self.services[service]
            if hasattr(scraper, "prepare_requests"):
                scraper.prepare_requests()

    def parse_raw_data(self):
        """Get raw data from all scrappers and prepare it for storing in DB."""
        for service in self.services_queue:
            if self.services[service]:
                scraper = self.services[service]
                temporary_data = scraper.get_data(
                    db_exchanges=self.db_exchanges
                )
                self.format_data[service] = temporary_data
                self.remove_exchanges_complite(temporary_data)

    def remove_exchanges_complite(self, data):
        """Remove id of exchanges with formated data from db_exchanges."""
        if "formated_exchanges_id" in data:
            self.db_exchanges.difference_update(data["formated_exchanges_id"])

    def check_exchanges_complite(self):
        """Check if all exchanges from db_exchanges got formated data."""
        if len(self.db_exchanges) > 0:
            from logger import ForbitLogs
            mess = (
                "{} exchanges did not get formated data. All list: {}"
            ).format(
                len(self.db_exchanges),
                self.db_exchanges
            )
            logger = ForbitLogs(__name__)
            logger.warn(mess)

    def get_format_data(self):
        """Return raw data."""
        self.import_scrapers()
        self.prepare_requests_data()
        self.parse_raw_data()
        self.check_exchanges_complite()
        return self.format_data
