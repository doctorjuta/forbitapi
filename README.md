# ForbitAPI

Scrapping crypto data for forbitchain site.

# DB data structure

## DB data structure for wp_options table

All data store in Wordpress DB talbe for options (as usual - wp_options). Key
of field consist of two parts: custom slug ("fbapi_") and service name. You
can get this data with simple Wordpress function get_option(key).

### Coingecko cryptocurrencies structure

Option key: fbapi_coingecko_coins

    [
        {
            "id": "bitcoin",
            "symbol": "BTC",
            "name": "Bitcoin",
            "price_usd": 0,
            "price_rub": 0,
            "price_btc": 0,
            "cap_usd": 0,
            "cap_usd_change_24": 0,
            "change_24h_prc": 0,
            "change_24h_usd": 0,
            "change_7d_prc": 0,
            "circulating_supply": 0,
            "total_supply": 0
        },
        ...
    ]

### Coingecko exchanges structure

Option key: fbapi_coingecko_exchanges

    [
        {
            "exchange": "bitforex",
            "pairs": [
                {
                    "base": "BTC",
                    "target": "USDT",
                    "name": "Bitcoin",
                    "price_usd": 0,
                    "volume": 0,
                    "volume_percent": 0
                },
                ...
            ],
            "volume": 0
        },
        ...
    ]

## DB data structure for individual tables

All data store in two different tables: db.COINS_TABLE and db.EXCHANGES_TABLE.
Each of them structure describe in db.COINS_TABLE_SQL and
db.EXCHANGES_TABLE_SQL params. Pay attention, that "pairs" field of
db.EXCHANGES_TABLE is a list of objects with next structure:

    "pairs": [
        {
            "base": "BTC",
            "target": "USDT",
            "name": "Bitcoin",
            "price_usd": 0,
            "volume": 0,
            "volume_percent": 0
        },
        ...
    ]

## Addition SQL requests

You need to run next sql requests on prod server after finished implementation
new API. This request change Exchanges api_slug from coinmarketcap.com slug to
exchanges id in coingecko.com API.

    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'coin_egg' WHERE `wp_postmeta`.`meta_id` = 48444
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'waves' WHERE `wp_postmeta`.`meta_id` = 20598
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'btc_alpha' WHERE `wp_postmeta`.`meta_id` = 16847
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'cex' WHERE `wp_postmeta`.`meta_id` = 11099
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'btc_trade_ua' WHERE `wp_postmeta`.`meta_id` = 24099
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'bitex' WHERE `wp_postmeta`.`meta_id` = 17024
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'crypto_bridge' WHERE `wp_postmeta`.`meta_id` = 44952
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'bancor' WHERE `wp_postmeta`.`meta_id` = 37287
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'coin_exchange' WHERE `wp_postmeta`.`meta_id` = 49087
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'coinbase_pro' WHERE `wp_postmeta`.`meta_id` = 47840
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'bit_z' WHERE `wp_postmeta`.`meta_id` = 39935
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'liquid' WHERE `wp_postmeta`.`meta_id` = 21872
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'south_xchange' WHERE `wp_postmeta`.`meta_id` = 20467
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'ccex' WHERE `wp_postmeta`.`meta_id` = 14386
    use `forbitchain`; UPDATE `wp_postmeta` SET `meta_value` = 'trade_satoshi' WHERE `wp_postmeta`.`meta_id` = 20267

We could not identify next exchanges slug from coinmarketcap.com:

* 'bisq'
* 'bitflip'
* 'bitmarket'
* 'bitmex'
* 'bitonic'
* 'bits-blockchain'
* 'coinfloor'
* 'coinrail'
* 'coinsquare'
* 'coinut'
* 'cryptomate'
* 'cryptox'
* 'gatehub'
* 'koinim'
* 'localtrade'
* 'okcoin-intl'
* 'tdax'
* 'therocktrading'
* 'xbtce'
* 'zebpay'
