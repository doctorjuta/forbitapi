"""Current library contain main logic for interaction with DB."""
import pymysql as pm
import phpserialize
from settings import DB_PARAM


class ForbitDB:
    """Main class for interaction with database."""

    OPTIONS_TABLE = "wp_options"
    PREFIX = "fbapi"

    COINS_TABLE = "{}_cripcoins".format(PREFIX)
    COINS_TABLE_SQL = (
        "CREATE TABLE IF NOT EXISTS {} ("
        "id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, "
        "coin_id VARCHAR(50) NOT NULL UNIQUE, "
        "symbol VARCHAR(10) NOT NULL, "
        "name VARCHAR(50) NOT NULL, "
        "price_usd VARCHAR(50), "
        "price_rub VARCHAR(50), "
        "price_btc VARCHAR(50), "
        "cap_usd VARCHAR(50), "
        "cap_usd_change_24 VARCHAR(50), "
        "change_24h_prc VARCHAR(50), "
        "change_24h_usd VARCHAR(50), "
        "change_7d_prc VARCHAR(50), "
        "circulating_supply VARCHAR(50), "
        "total_supply VARCHAR(50), "
        "timestamp INT"
        ")"
    ).format(
        COINS_TABLE
    )

    EXCHANGES_TABLE = "{}_exchanges".format(PREFIX)
    EXCHANGES_TABLE_SQL = (
        "CREATE TABLE IF NOT EXISTS {} ("
        "id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, "
        "exchange VARCHAR(50) NOT NULL UNIQUE, "
        "pairs LONGTEXT, "
        "volume FLOAT, "
        "timestamp INT"
        ")"
    ).format(
        EXCHANGES_TABLE
    )

    def __init__(self):
        """Init class."""
        self.connection = False
        self.create_tables_if_not_exist()

    def connect(self):
        """Create connection."""
        self.connection = pm.connect(**DB_PARAM)
        self.cursor = self.connection.cursor()

    def disconnect(self):
        """Remove connection."""
        self.connection.close()
        self.connection = False

    def runquery(self, query):
        """Execute provided query string."""
        self.cursor.execute(query)

    def runcommit(self):
        """Make the changes to the database persistent."""
        self.connection.commit()

    def write_options_data(self, service, data, autoload=False):
        """Write data from service in options table."""
        self.connect()
        key = "{}_{}".format(self.PREFIX, service)
        new_data = phpserialize.dumps(data).decode("utf-8")
        new_data = new_data.replace("'", "")
        autoload = "yes" if autoload else "no"
        sql = ("SELECT option_id FROM {} WHERE option_name='{}'").format(
            self.OPTIONS_TABLE,
            key
        )
        self.runquery(sql)
        row = self.cursor.fetchone()
        if row is not None:
            sql = (
                "UPDATE {} SET option_value='{}' WHERE option_id={}"
            ).format(
                self.OPTIONS_TABLE,
                new_data,
                row[0]
            )
        else:
            sql = (
                "INSERT INTO {} (option_name, option_value, autoload) "
                "VALUES ('{}', '{}', '{}')"
            ).format(
                self.OPTIONS_TABLE,
                key,
                new_data,
                autoload
            )
        self.runquery(sql)
        self.runcommit()
        self.disconnect()

    def write_data_in_own_table(self, data):
        """Write data from service in specific table."""
        if data["data_type"] == "coins":
            self.write_data_in_coin_table(data)
        elif data["data_type"] == "exchanges":
            self.write_data_in_exch_table(data)

    def write_data_in_coin_table(self, data):
        """Write data from service in coins table."""
        self.connect()
        if "data" in data and len(data["data"]) > 0:
            for row in data["data"]:
                sql = (
                    "SELECT id FROM {} WHERE coin_id='{}'".format(
                        self.COINS_TABLE,
                        row["id"]
                    )
                )
                self.runquery(sql)
                coin = self.cursor.fetchone()
                if coin is not None:
                    sql = (
                        "UPDATE {} SET price_usd='{}', price_rub='{}', "
                        "price_btc='{}', cap_usd='{}', "
                        "cap_usd_change_24='{}', change_24h_prc='{}', "
                        "change_24h_usd='{}', change_7d_prc='{}', "
                        "circulating_supply='{}', total_supply='{}', "
                        "timestamp={} WHERE id={}"
                    ).format(
                        self.COINS_TABLE,
                        row["price_usd"],
                        row["price_rub"],
                        row["price_btc"],
                        row["cap_usd"],
                        row["cap_usd_change_24"],
                        row["change_24h_prc"],
                        row["change_24h_usd"],
                        row["change_7d_prc"],
                        row["circulating_supply"],
                        row["total_supply"],
                        data["timestamp"],
                        coin[0]
                    )
                else:
                    sql = (
                        "INSERT INTO {} ("
                        "coin_id, symbol, name, price_usd, price_rub, "
                        "price_btc, cap_usd, cap_usd_change_24, "
                        "change_24h_prc, change_24h_usd, "
                        "change_7d_prc, circulating_supply, total_supply, "
                        "timestamp) VALUES('{}', '{}', '{}', '{}', '{}', "
                        "'{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', {})"
                    ).format(
                        self.COINS_TABLE,
                        row["id"],
                        row["symbol"],
                        row["name"],
                        row["price_usd"],
                        row["price_rub"],
                        row["price_btc"],
                        row["cap_usd"],
                        row["cap_usd_change_24"],
                        row["change_24h_prc"],
                        row["change_24h_usd"],
                        row["change_7d_prc"],
                        row["circulating_supply"],
                        row["total_supply"],
                        data["timestamp"]
                    )
                self.runquery(sql)
                self.runcommit()
        self.disconnect()

    def write_data_in_exch_table(self, data):
        """Write data from service in exchanges tables."""
        if "data" in data and len(data["data"]) > 0:
            for exchange in data["data"]:
                if data["data"][exchange]:
                    self.write_exchange_with_pairs(
                        exchange,
                        data["data"][exchange],
                        data["timestamp"]
                    )

    def write_exchange_with_pairs(self, exchange, exchange_data, timestamp):
        """Insert or update exhange data."""
        self.connect()
        pairs = phpserialize.dumps(exchange_data["pairs"]).decode("utf-8")
        pairs = pairs.replace("'", "")
        sql = (
            "SELECT id FROM {} WHERE exchange='{}'".format(
                self.EXCHANGES_TABLE,
                exchange
            )
        )
        self.runquery(sql)
        exchange_from_db = self.cursor.fetchone()
        if exchange_from_db is not None:
            exchange_id = exchange_from_db[0]
            sql = (
                "UPDATE {} SET pairs='{}', volume={}, timestamp={} WHERE id={}"
            ).format(
                self.EXCHANGES_TABLE,
                pairs,
                exchange_data["volume"],
                timestamp,
                exchange_id
            )
        else:
            sql = (
                "INSERT INTO {} (exchange, pairs, volume, timestamp) "
                "VALUES('{}', '{}', {}, {})"
            ).format(
                self.EXCHANGES_TABLE,
                exchange,
                pairs,
                exchange_data["volume"],
                timestamp
            )
        self.runquery(sql)
        self.runcommit()
        self.disconnect()

    def create_tables_if_not_exist(self):
        """Create all tables for data if they does not exist."""
        self.create_table_if_not_exist(
            self.COINS_TABLE,
            self.COINS_TABLE_SQL
        )
        self.create_table_if_not_exist(
            self.EXCHANGES_TABLE,
            self.EXCHANGES_TABLE_SQL
        )

    def create_table_if_not_exist(self, table_name, table_create_sql):
        """Create individual table if it does not exist."""
        self.connect()
        sql = (
            "SHOW TABLES LIKE '{}'"
        ).format(
            table_name
        )
        self.runquery(sql)
        table = self.cursor.fetchone()
        if table is None:
            self.runquery(table_create_sql)
            self.runcommit()
        self.disconnect()

    def get_cryptocurrencies(self):
        """Get list of cryptocurrencies from site DB."""
        cryptocurrencies = []
        self.connect()
        sql = (
            "SELECT meta_value, post_id FROM wp_postmeta LEFT JOIN "
            "wp_posts ON wp_postmeta.post_id=wp_posts.ID WHERE "
            "wp_postmeta.meta_key='coin_code' AND "
            "wp_posts.post_type='coins' AND "
            "wp_posts.post_status='publish' AND "
            "wp_postmeta.meta_value!=''"
        )
        self.runquery(sql)
        result = self.cursor.fetchall()
        for row in result:
            cryptocurrencies.append(row[0].lower())
        self.disconnect()
        return cryptocurrencies

    def get_currency_by_id(self, id):
        """Return currency from site DB by provided ID (BTC, ETH ...)."""
        currency = False
        self.connect()
        sql = (
            "SELECT post_title FROM wp_posts LEFT JOIN "
            "wp_postmeta ON wp_posts.ID=wp_postmeta.post_id WHERE "
            "wp_postmeta.meta_key='coin_code' AND "
            "wp_posts.post_type='coins' AND "
            "wp_posts.post_status='publish' AND "
            "wp_postmeta.meta_value='{}'"
        ).format(
            id
        )
        self.runquery(sql)
        result = self.cursor.fetchone()
        if result:
            currency = result[0]
        return currency

    def get_exchanges(self):
        """Get list of exchanges from site DB."""
        exchanges = []
        self.connect()
        sql = (
            "SELECT meta_value, post_id FROM wp_postmeta LEFT JOIN "
            "wp_posts ON wp_postmeta.post_id=wp_posts.ID WHERE "
            "wp_postmeta.meta_key='api_slug' AND "
            "wp_posts.post_type='exchanges' AND "
            "wp_posts.post_status='publish' AND "
            "wp_postmeta.meta_value!=''"
        )
        self.runquery(sql)
        result = self.cursor.fetchall()
        for row in result:
            exchanges.append(row[0].lower())
        self.disconnect()
        return exchanges

    def get_exchanges_data(self):
        """Get all exchanges with formated data from DB."""
        self.connect()
        sql = (
            "SELECT * FROM {}"
        ).format(
            self.EXCHANGES_TABLE
        )
        self.runquery(sql)
        result = self.cursor.fetchall()
        self.disconnect()
        return result

    def get_coins_data(self):
        """Get all cryptocurrencies with formated data from DB."""
        self.connect()
        sql = (
            "SELECT * FROM {}"
        ).format(
            self.COINS_TABLE
        )
        self.runquery(sql)
        result = self.cursor.fetchall()
        self.disconnect()
        return result
