"""Current library contain main logic for interaction with scraped data."""
from scrapers.scraper import ForbitScrap
from db import ForbitDB
from logger import ForbitLogs


class Forbit:
    """Main class for interaction with scraped data."""

    def __init__(self):
        """Init main class."""
        self.data = False

    def run(self):
        """Run all scripts."""
        self.retrive_data()
        self.write_data()

    def retrive_data(self):
        """Get formated data from API."""
        self.scraper = ForbitScrap()
        self.data = self.scraper.get_format_data()

    def write_data(self):
        """Write prepared data to DB."""
        if self.data:
            logger = ForbitLogs(__name__)
            logger.info(
                "Script is running"
            )
            for service in self.data:
                db = ForbitDB()
                # db.write_options_data(service, self.data[service])
                db.write_data_in_own_table(
                    self.data[service]
                )


if __name__ == "__main__":
    fb = Forbit()
    fb.run()
