from multiprocessing.dummy import Pool as ThreadPool
import time
import requests


api_path = "https://api.coingecko.com/api/v3/exchanges"


def get_exchanges():
    exchanges = []
    r = requests.get(api_path)
    if r.status_code == 200:
        r = r.json()
    else:
        print("Problem: {}".format(r))
        return exchanges
    if isinstance(r, list) and len(r) > 0:
        exchanges = [i["id"] for i in r]
    return exchanges


def run_single(id):
    path = "{}/{}".format(api_path, id)
    r = requests.get(path)
    if r.status_code == 200:
        r = r.json()
    else:
        print("Problem: {}".format(r))
    if r and "name" in r:
        pass
    else:
        print("Not name in json: {}".format(r))


def run_as_usual(ids):
    if len(ids) > 0:
        for id in ids:
            run_single(id)


def run_paralel(ids):
    if len(ids) > 0:
        pool = ThreadPool(4)
        pool.map(
            run_single,
            ids
        )
        pool.close()
        pool.join()


if __name__ == "__main__":
    ids = get_exchanges()
    start_time = time.time()
    run_as_usual(ids)
    print("As usual program work: {}".format(time.time()-start_time))
    start_time = time.time()
    run_paralel(ids)
    print("Paralel program work: {}".format(time.time()-start_time))
