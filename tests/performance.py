"""Library for script performance tests."""
import unittest
from main import Forbit


class ForbitTests(unittest.TestCase):
    """Main class for script performance tests."""

    def test_script_finished(self):
        """Test how much time take script running."""
        frb = Forbit()
        frb.run()
        self.assertIsInstance(
            frb,
            Forbit
        )
