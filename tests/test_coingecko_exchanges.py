"""List of applications tests for coingecko-exchanges API."""
import unittest
from scrapers.scraper import ForbitScrap
from db import ForbitDB
from scrapers.coingecko_exchanges.scraper import Scraper


skip_test = False
main_scraper = ForbitScrap()
if "coingecko_exchanges" not in main_scraper.services:
    skip_test = True


@unittest.skipIf(skip_test, "Coingecko Exchanges service is inactive")
class CoingeckoExchangesScrapingTests(unittest.TestCase):
    """Test of scraping process."""

    @classmethod
    def setUpClass(self):
        """Prepare all variables for testing."""
        self.db = ForbitDB()
        self.exchanges = self.db.get_exchanges()
        self.scraper = Scraper()

    def test_raw_data(self):
        """Test correctness of receiving data process."""
        raw_data = self.scraper.get_raw_data(
            self.exchanges[0]
        )
        self.assertTrue(raw_data)

    def test_formated_data(self):
        """Test correctness of formatting data process."""
        raw_data = self.scraper.get_raw_data(
            self.exchanges[0]
        )
        formated_data = self.scraper.get_format_data(
            self.exchanges[0],
            raw_data
        )
        self.assertTrue(formated_data)
