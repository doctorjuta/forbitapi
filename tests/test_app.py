"""List of applications tests for main script."""
import unittest
import time
from main import Forbit
from db import ForbitDB
from scrapers.scraper import ForbitScrap
from logger import ForbitLogs


class ForbitTests(unittest.TestCase):
    """Test of work of main script."""

    @classmethod
    def setUpClass(self):
        """Prepare all variables for testing."""
        self.db = ForbitDB()
        self.exchanges = self.db.get_exchanges()
        self.coins = self.db.get_cryptocurrencies()
        self.frb = Forbit()
        self.frb.run()

    def test_logging_base(self):
        """Test if logger object works properly."""
        import os
        path_to_test_file = "./logs/testlogs.log"
        logger = ForbitLogs("testlogs", "testlogs")
        logger.error("Test error message")
        self.assertTrue(os.path.isfile(path_to_test_file))
        os.remove(path_to_test_file)

    def test_libraries_import(self):
        """Test of import of all scraping libraries."""
        self.scraper = ForbitScrap()
        self.scraper.import_scrapers()
        for service in self.scraper.services:
            mess = "Can not import library for next service: {}".format(
                service
            )
            self.assertTrue(self.scraper.services[service], mess)

    def test_all_exchanges_wrote_in_db(self):
        """Test count of exchanges data from DB.

        Test count of exchanges data from DB equal to count of exchanges
        post type with api slug.
        """
        exchanges_data = self.db.get_exchanges_data()
        exchanges_id_list = [i[1] for i in exchanges_data]
        not_in_db = list(set(self.exchanges) - set(exchanges_id_list))
        mess = "Exchanges {} not exist in DB.".format(
            not_in_db
        )
        self.assertEqual(len(not_in_db), 0, mess)

    def test_all_exchanges_in_db_has_volume(self):
        """Test if all exchanges from DB has volume more than zero."""
        exchanges_data = self.db.get_exchanges_data()
        for exchange in exchanges_data:
            mess = "Exchange {} has 0 value of volume.".format(
                exchange[1]
            )
            self.assertTrue(exchange[3], mess)

    def test_all_exchanges_was_updated_last_hour(self):
        """Test if all exchanges from DB was updated in last one hour."""
        timestamp_minus_1h = int(time.time()) - (60 * 60)
        exchanges_data = self.db.get_exchanges_data()
        for exchange in exchanges_data:
            timestamp = int(exchange[-1])
            mess = (
                "Exchange {} was not updated during last hour."
            ).format(
                exchange[1]
            )
            self.assertTrue((timestamp > timestamp_minus_1h), mess)

    def test_all_cryptocur_in_data(self):
        """Test if all cryptocurrencies from DB exist in received data."""
        coins_data = self.db.get_coins_data()
        coins_symbol_list = [i[2] for i in coins_data]
        for coin in self.coins:
            mess = "Cryptocurrency {} does not exist in data".format(
                coin
            )
            self.assertIn(coin, coins_symbol_list, mess)

    def test_all_cryptocur_in_db_has_usd_rate(self):
        """Test if all cryptocurrencies from DB has value of USD price."""
        coins_data = self.db.get_coins_data()
        for coin in coins_data:
            price_usd = round(float(coin[4]), 6)
            mess = (
                "Cryptocurrency {} does not have USD price. Value: {}"
            ).format(
                coin[3],
                coin[4]
            )
            self.assertNotEqual(price_usd, 0.0, mess)

    def test_all_cryptocur_was_updated_last_hour(self):
        """Test if all coins from DB was updated in last one hour."""
        timestamp_minus_1h = int(time.time()) - (60 * 60)
        coins_data = self.db.get_coins_data()
        for coin in coins_data:
            timestamp = int(coin[-1])
            mess = (
                "Exchange {} was not updated during last hour."
            ).format(
                coin[3]
            )
            self.assertTrue((timestamp > timestamp_minus_1h), mess)
