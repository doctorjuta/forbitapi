"""List of applications tests for coingecko-coins API."""
import unittest
from scrapers.scraper import ForbitScrap
from scrapers.coingecko_coins.scraper import Scraper


skip_test = False
main_scraper = ForbitScrap()
if "coingecko_coins" not in main_scraper.services:
    skip_test = True


@unittest.skipIf(skip_test, "Coingecko Coins service is inactive")
class CoingeckoCoinsScrapingTests(unittest.TestCase):
    """Test of scraping process."""

    TARGET_FIELDS = [
        "symbol",
        "name",
        "market_data"
    ]

    @classmethod
    def setUpClass(self):
        """Prepare all variables for testing."""
        self.scraper = Scraper()
        self.data = self.scraper.get_data()

    def test_data_valid(self):
        """Test of correct data received from api."""
        raw_data = self.scraper.raw_data
        self.assertTrue(raw_data)
        self.assertIsInstance(raw_data, list)
        for field in self.TARGET_FIELDS:
            self.assertIn(field, raw_data[0])
