"""Logger library.

Current library create wrapper of default python logging functions.
"""
import logging
import logging.handlers
from settings import APP_PATH


class ForbitLogs:
    """Main class for logging process."""

    def __init__(self, process="Forbit", filename="forbit"):
        """Create main logger."""
        file_log_path = "{}/logs/{}.log".format(
            APP_PATH,
            filename
        )
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )
        self.logger = logging.getLogger(process)
        self.logger.setLevel(logging.DEBUG)
        file_log = logging.handlers.RotatingFileHandler(
            file_log_path,
            maxBytes=2097152,
            backupCount=5
        )
        file_log.setLevel(logging.DEBUG)
        file_log.setFormatter(formatter)
        self.logger.addHandler(file_log)

    def error(self, mess):
        """Write error message."""
        self.logger.error(mess)

    def debug(self, mess):
        """Write debug message."""
        self.logger.debug(mess)

    def info(self, mess):
        """Write information message."""
        self.logger.info(mess)

    def warn(self, mess):
        """Write warning message."""
        self.logger.warning(mess)
